package io.javabrains.topic;

/**
 * 
 * @author hmireles
 *
 */
public class Topic implements Comparable<Topic> {

	private String idTopic;
	private String name;
	private String description;

	public Topic() {
		// TODO Auto-generated constructor stub
	}

	public Topic(final String idTopic, final String name, final String description) {
		super();
		this.idTopic = idTopic;
		this.name = name;
		this.description = description;
	}

	public String getIdTopic() {
		return idTopic;
	}

	public void setIdTopic(final String idTopic) {
		this.idTopic = idTopic;
	}

	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int compareTo(Topic o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public boolean equals(Topic t){
		return this.getIdTopic().equals(t.getIdTopic());
	}

}
