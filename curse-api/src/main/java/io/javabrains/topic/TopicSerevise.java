package io.javabrains.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

@Service
public class TopicSerevise {

	private List<Topic> topics = new ArrayList<>(Arrays.asList(
					new Topic("Spring" , "Spring Framework " , "Spring framework desc "),
					new Topic("java", "java core", "java desc"),
					new Topic("JavaScript" , "name" , "description")				
				));
	
	private String nombre=null;
	
	public List<Topic> getAllTopics(){
		return topics;
	}
	
	public Topic getTopic(String id){
		return topics.stream().filter(t -> t.equals(id)).findFirst().get();
	}
	
	@SuppressWarnings("unused")
	private Topic filter(Stream<Topic> stream, Topic topic){
		Stream<Topic> objeto = stream.filter(t -> t.equals(topic));
		return getTopic2(objeto.findFirst());
	}
	
	private Topic getTopic2(Optional<Topic> optional){
		return optional.get();
	}

	public void addTopic(Topic topic) {
		topics.add(topic);
	}

	public void saveHello(String hola) {
		nombre="Hola mundo: "+hola;
		
	}

	public String getName() {
		// TODO Auto-generated method stub
		return nombre;
	}

	public void addTopic(String id, Topic topic) {
		for (int i = 0; i < topics.size(); i++) {
			Topic t = topics.get(i);
			if (t.getIdTopic().equals(id)) {
				topics.set(i, topic);
				return;
			}
		}
		
	}

	public void deleteTopic(String id) {
		topics.removeIf(t ->t.getIdTopic().equals(id));
	}
	
	
}
