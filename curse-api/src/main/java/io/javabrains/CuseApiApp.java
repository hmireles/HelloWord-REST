package io.javabrains;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuseApiApp {

	public static void main(final String[] args) {
		SpringApplication.run(CuseApiApp.class, args);
	}

}
